# popRocks = fn
#   {0, 0, _} -> "PopRocks"
#   {0, _, _} -> "Pop"
#   {_, 0, _} -> "Rocks"
#   {_,_,c} -> "this is third argument #{c}"
# end

# popRocks = fn
#   (0, 0, _) -> "PopRocks"
#   0, _, _ -> "Pop"
#   _, 0, _ -> "Rocks"
#   _,_,c -> "this is third argument #{c}"
# end
# IO.puts popRocks.(0, 0, 8)
# IO.puts popRocks.({0, 0, 8})
# IO.puts popRocks.({0, 1, 7})
# IO.puts popRocks.({2, 0, 3})
# IO.puts popRocks.({2, 1, 15})


# popRocks = fn
#   (tupl) ->
#     if {0, 0, _} = tupl do "catching" end
# end
# IO.puts popRocks.({0,0, 8})

# handle_open = fn
#   {:ok, file} -> "First line: #{IO.read(file, :line)}"
#   {_, error} -> "Error: #{:file.format_error(error)}"
#   end
#   IO.puts handle_open.(File.open("./functions-1.exs"))
#   IO.puts handle_open.(File.open("nonexistent.yml"))

popRocks = fn
  0, 0, _ -> "PopRocks"
  0, _, _ -> "Pop"
  _, 0, _ -> "Rocks"
  _,_,c -> "this is third argument #{c}"
end

something = fn
  x -> popRocks.(rem(x, 3), rem(x, 5), x)
end

10..16 |> Enum.each( fn(n) -> IO.puts something.(n) end)
# IO.puts something.(11)
# IO.puts something.(12)
# IO.puts something.(14)
# IO.puts something.(15)
# IO.puts something.(16)
