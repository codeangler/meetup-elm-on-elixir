Atoms

Atoms serve as constants, akin to Ruby’s symbols. Atoms always start with a colon. There are two different ways to create atoms. For example, both :hello_atom and :"Hello Atom" are valid atoms. Atoms are not the same as strings—they’re completely separate data type

iex> :hello_atom == "hello_atom"
false

Tuples

{200, "http://www.elixir-lang.org"}

Maps

iex> programmers = Map.new
%{}

iex> programmers = Map.put(programmers, :joe, "Erlang")
%{joe: "Erlang"}

iex> programmers = Map.put(programmers, :matz, "Ruby")
%{joe: "Erlang", matz: "Ruby"}

iex> programmers = Map.put(programmers, :rich, "Clojure")
%{joe: "Erlang", matz: "Ruby", rich: "Clojure"}

Keyword Lists
is a special list of two-element tuples whose first element is an atom; they share performance with lists:
```elixir
iex> [foo: "bar", hello: "world"]
[foo: "bar", hello: "world"]
iex> [{:foo, "bar"}, {:hello, "world"}]
[foo: "bar", hello: "world"]
```


----------


process all PID that way...

Dave Thomas  1.2 elixir

functional program
concurrency paradigm

care:
OO great single threaded code
concerns : multiple processes or threads, problems: shared state, locks/synchronosystion points

