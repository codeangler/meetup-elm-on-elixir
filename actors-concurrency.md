[ like Elm compiler?  but not ? ]
 The OTP framework is what gives BEAM languages (Erlang, Elixir, and so on) their superpowers, and it comes bundled with Elixir.

One of the most important concepts in OTP is the notion of behaviors. A behavior can be thought of as a contract between you and OTP.

When you use a behavior, OTP expects you to fill in certain functions. In exchange for that, OTP takes care of a slew of issues such as message handling (synchronous or asynchronous), concurrency errors (deadlocks and race conditions), fault tolerance, and failure handling. These issues are general—almost every respectable client/server program has to handle them somehow, but OTP steps in and handles all of these for you. Furthermore, these generic bits have been used in production and battle-tested for years.

(1.5.1)



reference: actor model
https://www.brianstorti.com/the-actor-model/

[Hewitt, Meijer and Szyperski: The Actor Model (everything you wanted to know...)](https://youtu.be/7erJ1DV_Tlo)


---------------------------

example: hot code swapping:

https://www.youtube.com/watch?v=CZWMc2cXUAw

## notes
Simple GenServer in iEx
minute 3....just a simple holding on of state

## version 1
  nexter.v1.ex
1. w/n iex>
``` bash
iex> c "nexter.v1.ex"
iex> {:ok, pid} = Nexter.start_link
iex> Nexter.next pid
```

## Version 2
just changing output ...

minute 4:45
``` bash
c "nexter.v2.ex"
```

### code is swapped ...

## Version 3
what if we want to change the state it is holding on to... maybe from a number to a tuple

no add message, {msg}, initial state {0, msg}

handle_call
***
!!!!
> now structure of {} won't match previous int, so there is a changing the record of current state

> needs to be a bit more guided

```elixir
  :sys.suspend pid

  c "nexter.v3.ex"

  :sys.change_code pid, Nexter, "2", nil

  :sys.resume pid

  Nexter.next pid, "hello, world!"

```

if there is no need to hold state, then


-------------------- 6.1.1  in action v 1

Spawn a separate process.
Run an infinite loop in the process.
Maintain the process state.
React to messages.
Send a response back to the caller.


"a module name is an atom"


