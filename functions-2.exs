# fizzBuzz = fn
#  (a, b, c) ->
#   cond do
#      a == 0 && b == 0 -> "FizBuz"
#      a == 0 -> "Fiz"
#      b == 0 -> "Buz"
#      true -> "#{c}"
#   end
# end

# fizzBuzz = fn
#   0, 0, _  -> "FizzBuzz"
#   0, _, _  -> "Fizz"
#   _, 0, _  -> "Buzz"
#   _, _, a3 ->  a3
#   end

# IO.puts fizzBuzz.(0,0,7)
# IO.puts fizzBuzz.(0, 9 ,7)
# IO.puts fizzBuzz.(2, 0, 3)
# IO.puts fizzBuzz.(2, 1, 3)

# popRocks = fn
#   (tupl) ->
#     if {0, 0, _} = tupl do "catching" end
# end
# IO.puts popRocks.({0,0, 8})

# popRocks = fn
#   {0, 0, _} -> "PopRocks"
#   {0, _, _} -> "Pop"
#   {_, 0, _} -> "Rocks"
#   # (t) -> if {_, _,c} = t do "this is third argument #{c}" end
#   {_,_,c} -> "this is third argument #{c}"
# end
# IO.puts popRocks.({0,0, 8})
# IO.puts popRocks.({0,1,7})
# IO.puts popRocks.({2, 0, 3})
# IO.puts popRocks.({2, 1, 15})


----------


# # popRocks = fn
#   {0, 0, _} -> "PopRocks"
#   {0, _, _} -> "Pop"
#   {_, 0, _} -> "Rocks"
#   {_,_,c} -> "this is third argument #{c}"
# # end

# popRocks = fn
#   (0, 0, _) -> "PopRocks"
#   0, _, _ -> "Pop"
#   _, 0, _ -> "Rocks"
#   _,_,c -> "this is third argument #{c}"
# end
# IO.puts popRocks.(0, 0, 8)
# IO.puts popRocks.({0, 0, 8})
# IO.puts popRocks.({0, 1, 7})
# IO.puts popRocks.({2, 0, 3})
# IO.puts popRocks.({2, 1, 15})


# popRocks = fn
#   (tupl) ->
#     if {0, 0, _} = tupl do "catching" end
# end
# IO.puts popRocks.({0,0, 8})

# handle_open = fn
#   {:ok, file} -> "First line: #{IO.read(file, :line)}"
#   {_, error} -> "Error: #{:file.format_error(error)}"
#   end
#   IO.puts handle_open.(File.open("./functions-1.exs"))
#   IO.puts handle_open.(File.open("nonexistent.yml"))

popRocks = fn
  0, 0, _ -> "PopRocks"
  0, _, _ -> "Pop"
  _, 0, _ -> "Rocks"
  _,_,c -> "this is third argument #{c}"
end

something = fn
  x -> popRocks.(rem(x, 3), rem(x, 5), x)
end

10..16 |> Enum.each( fn(n) -> IO.puts something.(n) end)
# IO.puts something.(11)
# IO.puts something.(12)
# IO.puts something.(14)
# IO.puts something.(15)
# IO.puts something.(16)
