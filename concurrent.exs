run_query = fn(query_def) ->
  :timer.sleep(2000)
  "#{query_def} result"
end

run_query.("query 1")

# 2 seconds later
# "query 1 result"



1..5 |>
Enum.map(&run_query.("query #{&1}"))

# 10 seconds later
# ["query 1 result", "query 2 result", "query 3 result", "query 4 result",
#  "query 5 result"]

spawn(fn -> IO.puts(run_query.("query 1987")) end)

# 2 seconds later
# "query 1987 result"

---------------------

async_query = fn(query_def) ->
  spawn(fn -> IO.puts(run_query.(query_def)) end)
end

async_query.("query 199")

# 2 seconds later
# query 199 result



Enum.each(1..5, &async_query.("query #{&1}"))
# :ok  # returned immediately

# 2 seconds later
# query 2 result
# query 1 result
# query 3 result
# query 5 result
# query 4 result
```
