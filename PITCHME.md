# Elixir
 _Just a Sip_

*Front Range Elm Meetup*

Boulder, CO

2018-10-24

Casey Burnett

+++?image=assets/casey-burnett-dino.jpg
@title[Casey Burnett]
@transition[none]

@snap[south-east]
@color[#2cd66b](Casey Burnett)
@snapend

@snap[south-west sidebar]
Sling TV
<br>
EMDR for Athletes
<br>
Achieve Goals Now
<br>
Rocky Mountain Anglers
@snapend

@snap[north-east sidebar]
@fa[twitter-square] @codeangler
<br>
@fa[external-link] codeangler.com
@snapend

+++
@title[Talk Objectives]

## Overview

@ul
- 10,000' History and Design Constraints
- 1' Pass by some Syntax
- 10' Pattern Matching
- **1,000' Concurrency**
- 5,000' Hot Reloading
@ulend

+++
@title[History Part 1]

## Birth of Elixir
@ul
- A functional, meta-programming-aware language built on top of the Erlang virtual machine

- Everything that Erlang is great for also applies to Elixir. Elixir and OTP combined provide facilities to build

- First appeared 2011

- Presently on v 1.7.3 as of August 2018
@ulend
+++

### Overloaded words
_Erlang & OTP & BEAM_
@ul
- Erlang was result of work in mid-1980s by Ericsson, a Swedish telecom giant
- Open Telecom Platform (OTP): OTP is a general-purpose framework and has little to do with telecom.
- BEAM : "Bogdan/Björn's Erlang Abstract Machine" - it is just the name of the VM
@ulend

+++

### What are the needs of a phone provider

+++
@title[Concurrent, Scalable, Fault-tolerant, Distributed programs]

### Beauty of Constraints
@ul
- Concurrent
- Scalable
- Fault-tolerant
- Distributed programs
@ulend
+++

## Use Cases

- Chat servers
- Game servers
- Web frameworks (Phoenix)
- Distributed databases (Riak and CouchDB)
- Real-time bidding servers
- Video-streaming services
- Long-running services/daemons
- Command-line applications

+++
## Users

- WhatsApp (Erlang)
- Pinterest (Elixir)
- Discord (Elixir)
- Parkifi (Elixir)
- SlingTV (Elixir)

+++

## Syntax

+++

### Built-in types
```yaml
Value types:
  Arbitrary-sized integers
  Floating-point numbers
  Atoms
  Ranges
  Regular expressions
System types:
  PIDs and ports
  References
Collection types:
  Tuples
  Lists
  Maps
  Binaries
Functions
```

@[10]
@[10-13]

+++

### Primitives

```markdown
| Sample                  | Type            |
| ---                     | ---             |
| `nil`                   | Nil/null        |
| `true` _/_ `false`      | Boolean         |
| ---                     | ---             |
| `?a`                    | Integer (ASCII) |
| `23`                    | Integer         |
| `3.14`                  | Float           |
| ---                     | ---             |
| `'hello'`               | Charlist        |
| `<<2, 3>>`              | Binary          |
| `"hello"`               | Binary string   |
| `:hello`                | Atom            |
```

@[13]

+++

### Primitives _continued_

```markdown
| Sample                  | Type            |
| ---                     | ---             |
| `[a, b]`                | List            |
| `{a, b}`                | Tuple           |
| ---                     | ---             |
| `%{a: "hello"}`         | Map             |
| `%MyStruct{a: "hello"}` | Struct          |
| `fn -> ... end`         | Function        |
```
@[3]
@[3]
@[6]
@[7]
@[8]

+++

#### Tuples

```elixir
t = { :a, :b }
```

```elixir
t |> elem(1) # -> :b   # like tuple[1]
t |> put_elem(1, :c) # -> {:a, :c}
t |> tuple_size() # -> 2
```
Tuples are similar to lists, but are stored contiguously in memory. This makes accessing their length fast but modification expensive;
+++

#### Keyword lists

```elixir
list = [{ :name, "John" }, { :age, 15 }]
list[:name] # -> "John"
```

```elixir
# For string-keyed keyword lists
list = [{"size", 2}, {"type", "shoe"}]
List.keyfind(list, "size", 0)  # -> {"size", 2}
```

A keyword list is a special list of two-element tuples whose first element is an atom; they share performance with lists

+++

#### Lists
Lists are simple collections of values which may include multiple types; lists may also include non-unique values:
```elixir
list = [3.14, :pie, "Apple"]
users = [ "Tom", "Dick", "Harry" ]
```

```elixir
Enum.map(users, fn user ->
  IO.puts "Hello " <> user
end)
# -> Hello Tom
# -> Hello Dick
# -> Hello Harry
# -> [:ok, :ok, :ok]
```

+++

#### Maps

```elixir
user = %{
  name: "John",
  city: "Melbourne"
}
```

```elixir
IO.puts "Hello, " <> user.name
# -> Hello, John
```
Unlike keyword lists, maps allow keys of any type and are un-ordered.
You can define a map with the %{} syntax
+++

## Pattern Matching

+++
@title[Pattern Matching def 1]

_Pattern Matching_

The left side is a list containing three variables, and the right is a list of three values, so the two sides could be made the same by setting the variables to the corresponding values.

+++
@title[Example Swap w/ Px Match]

```elixir
swap = fn { a, b } -> { b, a } end
  #Function<12.17052888 in :erl_eval.expr/5> ​
swap.({ 6, 8 }) ​
  {8, 6}
```

@[1]
@[3]
@[4]

+++
@title[FizzBuzz by conditional]

```elixir
fizzBuzz = fn
  (a, b, c) ->
   cond do
      a == 0 && b == 0 -> "FizzBuzz"
      a == 0 -> "Fizz"
      b == 0 -> "Buzz"
      true -> "#{c}"
   end
 end

 IO.puts fizzBuzz.(0, 0, 7) # FizzBuzz
 IO.puts fizzBuzz.(0, 9, 7) # Fizz
 IO.puts fizzBuzz.(2, 0, 3) # Buzz
 IO.puts fizzBuzz.(2, 1, 4) # 4

```
@[2, 11]
@[2, 4, 11]
@[2, 5, 12]
@[2, 6, 13]

+++
@title[Px Matching example tuples]

Pattern Matching _continued_

```elixir
popRocks = fn
  {0, 0, _} -> "PopRocks"
  {0, _, _} -> "Pop"
  {_, 0, _} -> "Rocks"
  {_, _, c} -> "This is third argument #{c}"
end
IO.puts popRocks.({0, 0, 8}) # PopRocks
IO.puts popRocks.({0, 1, 7}) # Pop
IO.puts popRocks.({2, 0, 3}) # Rocks
IO.puts popRocks.({2, 1, 15}) # This is third argument 15
```

@[2, 7]
@[3, 8]
@[4, 9]
@[5, 10]

+++
@title[Pattern Matching Def 2]

_Pattern Matching Review_

A pattern (the left side) is matched if the values (the right side) have the same structure and if each term in the pattern can be matched to the corresponding term in the values.

In Other Words:  Type & Quantity are same on either side of `=`

```elixir
swap = fn { a, b } -> { b, a } end
  #Function<12.17052888 in :erl_eval.expr/5> ​
swap.({ 6, 8 }) ​
  {8, 6}
```
+++

### Gotcha

+++
@title[Syntax Secrets]

@quote[Elixir will identify the type and not need syntax decoration]
```elixir
fizzBuzz = fn
  (a, b, c) ->
  ...
end
fizzBuzz = fn
  a, b, c ->
  ...
 end
IO.puts fizzBuzz.(0, 0, 8)

popRocks = fn
  {0, 0, _} -> "PopRocks"
  ...
end
IO.puts popRocks.({0, 0, 8})
```

@[2, 9]
@[6, 9]
@[2, 6, 9]
@[12, 15]

+++
@title[Elixir Doesn't Match Past Surface]

### Applesauce

```elixir
popRocks = fn
  0, 0, _ -> "PopRocks"
  0, _, _ -> "Pop"
  _, 0, _ -> "Rocks"
  _, _, c -> "this is third argument #{c}"
end
IO.puts popRocks.({0, 0, 8})
```

```bash
** (BadArityError) #Function<0.53305389 in file:scratchpad.exs> with arity 3 called with 1 argument ({0, 0, 8})
    scratchpad.exs:14: (file)
    (elixir) lib/code.ex:767: Code.require_file/2
```

_Lies. I tell you Lies._
@[2, 7]
@[8]
+++

### FizzBuzz with No Conditional Logic

```elixir
popRocks = fn
  0, 0, _ -> "PopRocks"
  0, _, _ -> "Pop"
  _, 0, _ -> "Rocks"
  _, _, c -> "this is third argument #{c}"
end

something = fn
  x -> popRocks.(rem(x, 3), rem(x, 5), x)
end

10..16 |> Enum.each( fn(n) -> IO.puts something.(n) end)
```

+++

### Common Use of _Pattern Matching_

+++
@title[check if file exisit example]

```elixir
handle_open = fn
  {:ok, file} -> "First line: #{IO.read(file, :line)}"
  {_, error} -> "Error: #{:file.format_error(error)}"
end

IO.puts handle_open.(File.open("./docker-compose.yml"))
# First line: version:3.5
IO.puts handle_open.(File.open("nonexistent.yml"))
# Error: no such file or directory
```

+++
@title[Blank Intentionally]

+++

## Breath

Full in 4 counts
<br>
Empty in 7 counts

### 3 times

+++
@title[Blank Intentionally]

+++

## Concurrency

+++
@title[BEAM does concurrency]

In BEAM, a process is a concurrent thread of execution. Two processes run concurrently and may therefore run in parallel, assuming at least two CPU cores are available. Unlike OS processes or threads, BEAM processes are lightweight concurrent entities handled by the VM, which uses its own scheduler to manage their concurrent execution.

+++
@title[Schedulers Per CPU by Default]

![](/assets/schedulers-per-cpu-core.png)

+++

### Example

+++
@title[Simulate Long Query]

Simulate a long running query

 ```elixir
iex> run_query = fn(query_def) ->
  :timer.sleep(2000)
  "#{query_def} result"
end

iex> run_query.("query 1")

# 2 seconds later
"query 1 result"
```
@[1-4]
@[6]
@[8-9]
+++
@title[Sync Long Queries]

```elixir
1..5 |>
Enum.map(&run_query.("query #{&1}"))

# 10 seconds later
["query 1 result", "query 2 result", "query 3 result", "query 4 result",
 "query 5 result"]
```
@[1-2]
@[4-6]
+++
@title[Create a process with spawn]

To create a process, you can use the auto-imported spawn/1 function:

Documentation [spawn/1](https://hexdocs.pm/elixir/Kernel.html#spawn/1)

```elixir
iex> spawn(fn -> IO.puts(run_query.("query 1987")) end)
#PID<0.121.0>    # immediate

# 2 seconds later
"query 1987 result"
```
@[1]
@[4-5]
+++
@title[wrapping func around spawn]

```elixir
iex> async_query = fn(query_def) ->
  spawn(fn -> IO.puts(run_query.(query_def)) end)
end

iex> async_query.("query 199")

# 2 seconds later
query 199 result
```
@[1-3]
@[5, 7-8]
+++
@title[Running 5 request concurrently 2 sec results]

#### Ta-da

```elixir
iex> Enum.each(1..5, &async_query.("query #{&1}"))
:ok  # returned immediately

# 2 seconds later
query 2 result
query 1 result
query 3 result
query 5 result
query 4 result
```

@[1-2]
@[4-9]

+++

### So What?

+++
@title[Pinterest Tech Quote]

@quote[We’ve also seen an improvement in code clarity. We’re converting our notifications system from Java to Elixir. The Java version used an Actor system and weighed in at around 10,000 lines of code. The new Elixir system has shrunk this to around 1000 lines. The Elixir based system is also faster and more consistent than the Java one and runs on half the number of servers.](Pinterest Engineering Blog)

+++

### I'm all alone in this world

+++
@title[Batteries Included w/ BEAM]

The OTP framework is what gives BEAM languages (Erlang, Elixir, and so on) their superpowers, and it comes bundled with Elixir.

One of the most important concepts in OTP is the notion of behaviors. A behavior can be thought of as a contract between you and OTP.

+++
@title[OTP handles messages]

When you use a behavior, OTP expects you to fill in certain functions. In exchange for that, OTP takes care of a slew of issues such as message handling (synchronous or asynchronous), concurrency errors (deadlocks and race conditions), fault tolerance, and failure handling. These issues are general—almost every respectable client/server program has to handle them somehow, but OTP steps in and handles all of these for you. Furthermore, these generic bits have been used in production and battle-tested for years.

+++
@title[sharing state between process with msg]

![](/assets/inter-process-async-messages.png)

+++
@title[Blank Intentionally]

+++

## Breath

Full in 4 counts
<br>
Empty in 7 counts

### 3 times

+++
@title[Blank Intentionally]

+++

## Hot Reloading

+++
@title[code: nexter.v1.ex]

```elixir
defmodule Nexter do
  use GenServer
  # ...
  def start_link do
    GenServer.start_link(__MODULE__, 0)
  end

  def next(pid) do
    GenServer.call(pid, :next)
  end

  def handle_call(:next, _from, n) do
    {:reply, n, n + 1}
  end
end
```
_nexter.v1.ex_


+++
@title[code: nexter.v2.ex]

```elixir
defmodule Nexter do
  use GenServer
  # ...
  def start_link do
    GenServer.start_link(__MODULE__, 0)
  end

  def next(pid) do
    GenServer.call(pid, :next)
  end

  def handle_call(:next, _from, n) do
    {:reply, "here: #{n}", n + 1}
  end
end
```

_nexter.v2.ex_

+++
@title[code: nexter.v3.ex]

```elixir
defmodule Nexter do
  use GenServer
  # ...
  def start_link(msg) do
    GenServer.start_link(__MODULE__, {0, msg})
  end

  def next(pid, msg) do
    GenServer.call(pid, {:next, msg})
  end

  def handle_call({:next, msg}, _from, {n, prev_msg}) do
    new_state = {n + 1, msg}
    {:reply, "here: #{n}. Prev person said #{prev_msg}", new_state}
  end

  def code_change("2", n, _extra) do
    new_state = {n, "nothing"}
    {:ok, new_state}
  end
end
```
_nexter.v3.ex_
@[4-5]
@[8-9]
@[12-14]
@[17-19]
+++
@title[example cmds for swapping code]

```bash
:sys.suspend pid

c "nexter.v3.ex"

:sys.change_code pid, Nexter, "2", nil

:sys.resume pid

Nexter.next pid, "hello, world!"

```

+++
@title[Blank Intentionally]

+++
@title[Ask for feedback]

## Comments

@ul
- Comment on Content
- What else would you like to learn on Elixir
- Request for Feedback
@ulend

+++
@title[Blank Intentionally]

+++

### References

- ^1 Little elixir and otp guidebook _manning pub_
- ^2 Elixir in Action _manning pub_
- ^3 Programming Elixir >= 1.6  _prag programming_
- ^4 [WhatsApp Hired 50 Engineers](https://www.wired.com/2015/09/whatsapp-serves-900-million-users-50-engineers/) _wired mag_
- ^5 [Pinterest](https://medium.com/@Pinterest_Engineering/introducing-new-open-source-tools-for-the-elixir-community-2f7bb0bb7d8c) _Pinterest Eng Blog_

+++

### References

- ^6 [2018 List of Companies Using Elixir/Erlang](https://codesync.global/media/successful-companies-using-elixir-and-erlang/)
- ^7 [Elixir Cheetsheet](https://github.com/rstacruz/cheatsheets/blob/master/elixir.md) _Devhints.io_
- ^8 [Rosettacode.org](http://www.rosettacode.org/wiki/Array_length#Elm)
- ^9 [Phoenix 2 million Websockets](https://phoenixframework.org/blog/the-road-to-2-million-websocket-connections)
- ^10 [Elixir School.com](http://www.elixirschool.com)

+++
### References

- ^11 [Complete Elixir & Phoenix Bootcamp & Tutorial](https://www.udemy.com/share/1005SSAEsZcVZbQ3o=/) _Stephen Grider_ on Udemy
- ^12 [Discord App Scaled to 5mil Users](https://blog.discordapp.com/scaling-elixir-f9b8e1e7c29b) _CTO of Discord_
- ^13 [Elixir Recipes - Anonymous Functions](http://elixir-recipes.github.io/functions/anonymous-functions/)