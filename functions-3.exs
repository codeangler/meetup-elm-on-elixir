popRocks = fn
  0, 0, _ -> "PopRocks"
  0, _, _ -> "Pop"
  _, 0, _ -> "Rocks"
  # (t) -> if _, _,c = t do "this is third argument #c" end
  _,_,c -> "this is third argument #{c}"
end

# something = fn
#   x -> rem(x, 3)
# end

something = fn
  x -> popRocks.(rem(x, 3), rem(x, 5), x)
end

IO.puts something.(10)
IO.puts something.(11)
IO.puts something.(12)
IO.puts something.(14)
IO.puts something.(15)
IO.puts something.(16)
