
CONCURRENCY VS. PARALLELISM
It’s important to realize that concurrency doesn’t necessary imply parallelism. Two concurrent things have independent execution contexts. But this doesn’t mean they will run in parallel. If you run two CPU-bound concurrent tasks and only one CPU core, then parallel execution can’t happen.

Of course, by adding more CPU cores and relying on an efficient concurrent framework, you can achieve parallelism. But you should be aware that concurrency itself doesn’t necessarily speed things up.

----
Simulate a long running query
 ```elixir
iex> run_query = fn(query_def) ->
  :timer.sleep(2000)
  "#{query_def} result"
end

iex> run_query.("query 1")

# 2 seconds later
"query 1 result"
```

```elixir
1..5 |>
Enum.map(&run_query.("query #{&1}"))

# 10 seconds later
["query 1 result", "query 2 result", "query 3 result", "query 4 result",
 "query 5 result"]
```

```elixir
iex> spawn(fn -> IO.puts(run_query.(query_def)) end)
#PID<0.121.0>    # immediate

# 2 seconds later
"query 1987 result"
```

```elixir
iex> async_query = fn(query_def) ->
  spawn(fn -> IO.puts(run_query.(query_def)) end)
end

iex> async_query.("query 199")

# 2 seconds later
query 199 result
```

```elixir
iex> Enum.each(1..5, &async_query.("query #{&1}"))
:ok  # returned immediately

# 2 seconds later
query 2 result
query 1 result
query 3 result
query 5 result
query 4 result
```

```elixir

```

```elixir

```

```elixir

```
